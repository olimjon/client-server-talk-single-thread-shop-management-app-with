﻿namespace Client
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.nudAmount = new System.Windows.Forms.NumericUpDown();
            this.tbxShopCode = new System.Windows.Forms.TextBox();
            this.lblShopCode = new System.Windows.Forms.Label();
            this.lblAmount = new System.Windows.Forms.Label();
            this.lblDeliveryDate = new System.Windows.Forms.Label();
            this.dtpDeliveryDate = new System.Windows.Forms.DateTimePicker();
            this.btnSend = new System.Windows.Forms.Button();
            this.tbxRemotePort = new System.Windows.Forms.TextBox();
            this.tbxRemoteIp = new System.Windows.Forms.TextBox();
            this.lblRemoteIp = new System.Windows.Forms.Label();
            this.lblRemotePort = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.nudAmount)).BeginInit();
            this.SuspendLayout();
            // 
            // nudAmount
            // 
            this.nudAmount.Location = new System.Drawing.Point(93, 90);
            this.nudAmount.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.nudAmount.Name = "nudAmount";
            this.nudAmount.Size = new System.Drawing.Size(120, 20);
            this.nudAmount.TabIndex = 3;
            // 
            // tbxShopCode
            // 
            this.tbxShopCode.Location = new System.Drawing.Point(93, 64);
            this.tbxShopCode.MaxLength = 10;
            this.tbxShopCode.Name = "tbxShopCode";
            this.tbxShopCode.Size = new System.Drawing.Size(120, 20);
            this.tbxShopCode.TabIndex = 2;
            // 
            // lblShopCode
            // 
            this.lblShopCode.AutoSize = true;
            this.lblShopCode.Location = new System.Drawing.Point(9, 71);
            this.lblShopCode.Name = "lblShopCode";
            this.lblShopCode.Size = new System.Drawing.Size(60, 13);
            this.lblShopCode.TabIndex = 2;
            this.lblShopCode.Text = "Shop Code";
            // 
            // lblAmount
            // 
            this.lblAmount.AutoSize = true;
            this.lblAmount.Location = new System.Drawing.Point(9, 97);
            this.lblAmount.Name = "lblAmount";
            this.lblAmount.Size = new System.Drawing.Size(43, 13);
            this.lblAmount.TabIndex = 3;
            this.lblAmount.Text = "Amount";
            // 
            // lblDeliveryDate
            // 
            this.lblDeliveryDate.AutoSize = true;
            this.lblDeliveryDate.Location = new System.Drawing.Point(9, 122);
            this.lblDeliveryDate.Name = "lblDeliveryDate";
            this.lblDeliveryDate.Size = new System.Drawing.Size(71, 13);
            this.lblDeliveryDate.TabIndex = 4;
            this.lblDeliveryDate.Text = "Delivery Date";
            // 
            // dtpDeliveryDate
            // 
            this.dtpDeliveryDate.Location = new System.Drawing.Point(93, 116);
            this.dtpDeliveryDate.Name = "dtpDeliveryDate";
            this.dtpDeliveryDate.Size = new System.Drawing.Size(120, 20);
            this.dtpDeliveryDate.TabIndex = 4;
            // 
            // btnSend
            // 
            this.btnSend.Location = new System.Drawing.Point(12, 142);
            this.btnSend.Name = "btnSend";
            this.btnSend.Size = new System.Drawing.Size(201, 23);
            this.btnSend.TabIndex = 5;
            this.btnSend.Text = "Send";
            this.btnSend.UseVisualStyleBackColor = true;
            this.btnSend.Click += new System.EventHandler(this.btnSend_Click);
            // 
            // tbxRemotePort
            // 
            this.tbxRemotePort.Location = new System.Drawing.Point(93, 38);
            this.tbxRemotePort.Name = "tbxRemotePort";
            this.tbxRemotePort.Size = new System.Drawing.Size(120, 20);
            this.tbxRemotePort.TabIndex = 1;
            this.tbxRemotePort.Text = "2017";
            // 
            // tbxRemoteIp
            // 
            this.tbxRemoteIp.Location = new System.Drawing.Point(93, 12);
            this.tbxRemoteIp.Name = "tbxRemoteIp";
            this.tbxRemoteIp.Size = new System.Drawing.Size(120, 20);
            this.tbxRemoteIp.TabIndex = 0;
            this.tbxRemoteIp.Text = "127.0.0.1";
            // 
            // lblRemoteIp
            // 
            this.lblRemoteIp.AutoSize = true;
            this.lblRemoteIp.Location = new System.Drawing.Point(9, 19);
            this.lblRemoteIp.Name = "lblRemoteIp";
            this.lblRemoteIp.Size = new System.Drawing.Size(57, 13);
            this.lblRemoteIp.TabIndex = 7;
            this.lblRemoteIp.Text = "Ip Address";
            // 
            // lblRemotePort
            // 
            this.lblRemotePort.AutoSize = true;
            this.lblRemotePort.Location = new System.Drawing.Point(9, 45);
            this.lblRemotePort.Name = "lblRemotePort";
            this.lblRemotePort.Size = new System.Drawing.Size(66, 13);
            this.lblRemotePort.TabIndex = 7;
            this.lblRemotePort.Text = "Port Number";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(225, 172);
            this.Controls.Add(this.lblRemotePort);
            this.Controls.Add(this.lblRemoteIp);
            this.Controls.Add(this.tbxRemoteIp);
            this.Controls.Add(this.tbxRemotePort);
            this.Controls.Add(this.btnSend);
            this.Controls.Add(this.dtpDeliveryDate);
            this.Controls.Add(this.lblDeliveryDate);
            this.Controls.Add(this.lblAmount);
            this.Controls.Add(this.lblShopCode);
            this.Controls.Add(this.tbxShopCode);
            this.Controls.Add(this.nudAmount);
            this.Name = "Form1";
            this.Text = "Client";
            ((System.ComponentModel.ISupportInitialize)(this.nudAmount)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.NumericUpDown nudAmount;
        private System.Windows.Forms.TextBox tbxShopCode;
        private System.Windows.Forms.Label lblShopCode;
        private System.Windows.Forms.Label lblAmount;
        private System.Windows.Forms.Label lblDeliveryDate;
        private System.Windows.Forms.DateTimePicker dtpDeliveryDate;
        private System.Windows.Forms.Button btnSend;
        private System.Windows.Forms.TextBox tbxRemotePort;
        private System.Windows.Forms.TextBox tbxRemoteIp;
        private System.Windows.Forms.Label lblRemoteIp;
        private System.Windows.Forms.Label lblRemotePort;
    }
}

