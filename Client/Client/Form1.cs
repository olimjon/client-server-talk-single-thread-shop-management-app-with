﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Client
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnSend_Click(object sender, EventArgs e)
        {
            try
            {
                var sendThread = new BackgroundWorker();
                sendThread.DoWork += SendMessage;
                sendThread.RunWorkerAsync();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }


        private void SendMessage(object sender, DoWorkEventArgs e)
        {
            try
            {
                var client = new TcpClient();
                client.Connect(tbxRemoteIp.Text, Convert.ToInt32(tbxRemotePort.Text));
                using (var writer = new BinaryWriter(client.GetStream()))
                {
                    writer.Write(dtpDeliveryDate.Value.ToShortDateString().Length);
                    writer.Write(dtpDeliveryDate.Value.ToShortDateString().ToCharArray());
                    writer.Write(tbxShopCode.Text.Length);
                    writer.Write(tbxShopCode.Text.ToCharArray());
                    writer.Write(nudAmount.Value);
                    writer.Flush();

                    //get responce
                    using (var reader = new BinaryReader(client.GetStream()))
                    {
                        if (reader.ReadBoolean())
                        {
                            //clean fields to indicate success
                            tbxShopCode.Text = String.Empty;
                            nudAmount.Value = 0;
                            dtpDeliveryDate.ResetText();
                            MessageBox.Show("Success"); //If everything was accomplished as expected
                        }
                        else
                        {
                            MessageBox.Show("Fail"); //If anything on the server side was failed
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
