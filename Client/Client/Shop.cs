﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client
{
    class Shop
    {
        public int id { get; set; }
        public string shopCode { get; set; }
        public int amount { get; set; }
        public DateTime deliveryDate { get; set; }
    }
}
