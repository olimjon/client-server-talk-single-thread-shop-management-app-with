﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Server
{
    public partial class Form1 : Form
    {
        private int _localPortNumber;
        private Thread _listenerThread;
        private bool _listenerStarted;
        private TcpListener _tcpListener;
        private bool _requestStatus;
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(Object sender, EventArgs e)
        {
            CheckForIllegalCrossThreadCalls = false;

            //get local address information
            _localPortNumber = 2017;
            lblLocalAddress.Text = string.Format("{0}:{1}", GetLocalIP(), _localPortNumber);

            _listenerStarted = true;
            _tcpListener = new TcpListener(IPAddress.Any, _localPortNumber);

            //separate thread to handle incoming requests
            _listenerThread = new Thread(ListenerThreadMethod);
            _listenerStarted = true;
            _tcpListener.Start();
            _listenerThread.Start();
        }

        private string GetLocalIP()
        {
            var host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    return ip.ToString();
                }
            }
            return "?";
        }

        private void ListenerThreadMethod()
        {
            while (_listenerStarted)
            {
                try
                {
                    var handlerSocket = _tcpListener.AcceptSocket();
                    if (handlerSocket.Connected)
                    {
                        ThreadPool.QueueUserWorkItem(HandleConnection, handlerSocket);
                    }

                }
                catch (Exception)
                {
                }
            }
        }

        private void HandleConnection(object socketToProcess)
        {
            var handlerSocket = socketToProcess as Socket;
            if ((handlerSocket != null))
            {
                var networkStream = new NetworkStream(handlerSocket);
                // get the stream from that socket

                //find out what is wanted
                using (var reader = new BinaryReader(networkStream))
                {
                    int deliveryDateLength = reader.ReadInt32();
                    string deliveryDate = new string(reader.ReadChars(deliveryDateLength));
                    int shopCodeLength = reader.ReadInt32();
                    string shopCode = new string(reader.ReadChars(shopCodeLength));
                    int amount = reader.ReadInt32();

                    try
                    {
                        OleDbConnection con = new OleDbConnection();
                        con.ConnectionString = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=..\..\..\shop.accdb";
                        con.Open();
                        OleDbCommand cmd = new OleDbCommand("insert into Shop(ShopCode,Amount,DeliveryDate)values('" + shopCode + "'," + amount + ",'" + deliveryDate + "')", con);
                        cmd.ExecuteNonQuery();
                        con.Close();
                        _requestStatus = true;
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                        _requestStatus = false;

                    }

                    lbxStatus.Items.Add(string.Format("{0} - {1}", handlerSocket.RemoteEndPoint, 
                        "Shop Code: " + shopCode + " Amount: " + amount.ToString() + " Delivery Date: " + deliveryDate.ToString()));

                    using (var writer = new BinaryWriter(networkStream))
                    {
                        writer.Write(_requestStatus); //indicates that the message was recieved successfully
                        writer.Flush();
                    }
                }

                handlerSocket = null; // nullify socket
            }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            StopListening();
        }
        private void StopListening()
        {
            _listenerStarted = false;
            _tcpListener.Stop();
            _listenerThread.Interrupt();
            _listenerThread.Abort();
        }
    }
}
